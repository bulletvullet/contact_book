let currentObjId = undefined;
let stateData = undefined;

$(function () {
    $.ajax({
      url: "/api/contacts/",
      context: document.body
    }).done((data) => renderTable(data));
    $('#change').click(function(e) {
          e.preventDefault();
          const form = new FormData();
          const adr = new FormData();
          const address = {
              "street": $('#street').val(),
              "city": $('#city').val(),
              "country": $('#country').val(),
          };
          adr.append("street",$('#street').val());
          adr.append("city", $('#city').val());
          adr.append("country", $('#country').val());
          const img = $('#imageUpload').prop('files')[0];
          img ? form.append("image", img) : null;
          form.append("name", $('#name').val());
          form.append("tags", []);
          // form.append("address", JSON.stringify(address));
          // form.append("address", adr);
          form.append("last_name", $('#last_name').val());
          console.log(...form);
          // form.append("tags", JSON.stringify($('#tags').val().replace(/ /g,'').split(',')));
          // form.append("phone_numbers", JSON.stringify($('#telephone').val().replace(/ /g,'').split(',')));
          // form.append("urls", JSON.stringify($('#homepage').val().replace(/ /g,'').split(',')));
            const data = {
                "id": 26,
                "address": {
                    "city": "Kharkov",
                    "country": "Украина",
                    "street": "Klochkovskaya 193 A, 19/6"
                },
                "tags": [],
                "name": "Eduard 11111",
                "last_name": "Kucheryaviy",
                "image": null,
                "telephone": "+380982853173",
                "url": "google.com"
            };
          $.ajax({
              type: 'put',
              url: `/api/contacts/26/`,
              data: form,
              // data: JSON.stringify(data),
              contentType: false,
              // contentType: 'application/json',
              processData:false,
                statusCode: {
                    // 200: function (data) {location.reload()},
                }
            });
      });
    $('#search').click(function(e) {
         e.preventDefault();
          $("#contactTable tbody tr").remove();
        const search = $('#searchText').val();
        $.ajax({
            url: `/api/contacts/?search=${search}`,
          context: document.body
        }).done((data) => renderTable(data));
      });
    $('#reset').click(function(e) {
          $("#avatar").attr('src', '#');
      });
    $('#createForm').submit(function(e) {
      e.preventDefault();
      const form = new FormData();
      const img = $('#imageUpload').prop('files')[0];
      img ? form.append("image", img) : null;
      form.append("name", $('#name').val());
      form.append("last_name", $('#last_name').val());
      form.append("tags", JSON.stringify($('#tags').val().replace(/ /g,'').split(',')));
      form.append("phone_numbers", JSON.stringify($('#telephone').val().replace(/ /g,'').split(',')));
      form.append("urls", JSON.stringify($('#homepage').val().replace(/ /g,'').split(',')));
      $.ajax({
      type: 'post',
      url: '/api/contacts/',
      data: form,
      processData: false,
      contentType: false,
      statusCode: {
            201: function (data) {location.reload()},
        }
    });
     });
});

function renderTable(data) {
    stateData = data;
  data.map((obj, key) => {
      const tAdrs = obj.address;
      const sAdrs = tAdrs ? `${tAdrs.street};${tAdrs.city};${tAdrs.country}` : '';
      $('.table > tbody:last-child').append(
          `<tr id=${obj.id} onclick='return setActive(this);'>
             <th scope="row">${key + 1}</th>
             <td class="tdName">${obj.name}</td>
             <td class="tdLastName">${obj.last_name}</td>
             <td class="tdAdrs">${sAdrs}</td>
             <td class="tdPhone">${obj.phone_numbers}</td>
             <td class="tdTags">${obj.tags}</td>
             <td class="tdUrls">${obj.urls}</td>
             <td><span onclick='return deleteContact(${obj.id});' class="glyphicon glyphicon-remove"></span></p></td>
           </tr>`);
  })
}

function changeForm(data) {
    const currentData = stateData.filter((obj) => obj.id === parseInt(currentObjId, 10))[0];
    const tempAdrs = data[2].split(';');
    $('#name').val(data[0]);
    $('#last_name').val(data[1]);
    $('#street').val(tempAdrs[0]);
    $('#city').val(tempAdrs[1]);
    $('#country').val(tempAdrs[2]);
    $('#telephone').val(data[3]);
    $('#tags').val(data[4]);
    $('#homepage').val(currentData.urls);
    $('#avatar').removeAttr('hidden');
    $('#avatar').attr('src', currentData.image);
}

function setActive(elem) {
    currentObjId = $(elem).attr('id');
    const data = [
        $(elem).find('.tdName').html(),
        $(elem).find('.tdLastName').html(),
        $(elem).find('.tdAdrs').html(),
        $(elem).find('.tdPhone').html(),
        $(elem).find('.tdTags').html(),
    ];
    $(elem).parent().children("tr").removeClass('active');
    $(elem).addClass("active");
    changeForm(data);
}

function deleteContact(idContact) {
    $.ajax({
        type: 'DELETE',
        url: `/api/contacts/${idContact}/`,
         statusCode: {
            204: function (data) {location.reload()},
        }
    });
}


// CRSF validation
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});