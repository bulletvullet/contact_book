FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN mkdir /web

WORKDIR /web

ADD . /web/

RUN pip install -r requirements.txt

CMD python manage.py makemigrations && python manage.py migrate && python manage.py loaddata fixture.json && python manage.py runserver 0.0.0.0:8000