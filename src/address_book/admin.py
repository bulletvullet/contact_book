from django.contrib import admin
from .models import Contact, Address


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    fields = (
        'name',
        "last_name",
        'image',
        'address',
        'telephone',
        'url',
        'tags',
    )
    search_fields = ('name', 'last_name')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    fields = (
        'street',
        "city",
        'country',
    )
    search_fields = fields
