from django.apps import AppConfig


class AddressBookConfig(AppConfig):
    name = 'src.address_book'
