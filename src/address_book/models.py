from django.db import models
from django.core.validators import RegexValidator
from taggit.managers import TaggableManager

validators = {
    'phone': RegexValidator(regex=r'^\+?1?\d{9,15}$',
                            message="Phone number must be next format: +999999999'. Up to 15 digits allowed."),
    'url': RegexValidator(regex=r'\.', message="Url must be the next syntax: google.com, www.google.com")
}


class Address(models.Model):
    street = models.CharField(max_length=200,)
    city = models.CharField(max_length=200,)
    country = models.CharField(max_length=200,)

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

    def __str__(self):
        return f'{self.street}/{self.city}/{self.country}'


class Contact(models.Model):
    name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.ForeignKey(Address, null=True, blank=True, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='image', default='image/default.png', null=True, blank=True)
    telephone = models.CharField(validators=[validators['phone']], max_length=17, blank=True)
    url = models.CharField(validators=[validators['url']], max_length=100, blank=True)
    tags = TaggableManager(blank=True)

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"
        unique_together = ('name', 'last_name',)

    def __str__(self):
        return f'{self.name} {self.last_name}'
