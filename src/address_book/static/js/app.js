let currentObjId = undefined;
let stateData = undefined;

$(function () {
    $.ajax({
      url: "/api/contacts/",
      context: document.body
    }).done((data) => renderTable(data));
    $('#search').click(function(e) {
         e.preventDefault();
          $("#contactTable tbody tr").remove();
        const search = $('#searchText').val();
        $.ajax({
            url: `/api/contacts/?search=${search}`,
          context: document.body
        }).done((data) => renderTable(data));
      });
    $('#saveForm').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var method = 'PUT';
        var url = `/api/contacts/${currentObjId}/?format=json`;
        handleForm(form, method, url);
    });
    $('#createForm').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var method = 'POST';
        var url = `/api/contacts/?format=json`;
        handleForm(form, method, url);
    });
});

function handleForm(form, method, url) {
    const formedData = new FormData(form[0]);
    const fTags = formedData.get('tags');
    if (fTags) {
        const tags = JSON.stringify(fTags.replace(/\s/g, '').split(','));
        formedData.set('tags', tags);
    }
    $.ajax({
      method: method,
      url: url,
      data: formedData,
      contentType: false,
      processData: false,
        dataType: 'json',
      headers: {
          'Accept': 'text/html; q=1.0, */*'
      },
      statusCode: {
        200: function (data) {location.reload()},
        201: function (data) {location.reload()},
      },
      error: function (jqXHR, textStatus, errorThrown) {
          alert(jqXHR.responseText);
      }
    });
}

function renderTable(data) {
    stateData = data;
  data.map((obj, key) => {
      const tAdrs = obj.address;
      const sAdrs = tAdrs ? `${tAdrs.street};${tAdrs.city};${tAdrs.country}` : '';
      $('.table > tbody:last-child').append(
          `<tr id=${obj.id} onclick='return setActive(this);'>
             <th scope="row">${key + 1}</th>
             <td class="tdName">${obj.name}</td>
             <td class="tdLastName">${obj.last_name}</td>
             <td class="tdAdrs">${sAdrs}</td>
             <td class="tdPhone">${obj.telephone}</td>
             <td class="tdTags">${obj.tags}</td>
             <td class="tdUrls">${obj.url}</td>
             <td><span onclick='return deleteContact(${obj.id});' class="glyphicon glyphicon-remove"></span></p></td>
           </tr>`);
  })
}

function changeForm(data) {
    const currentData = stateData.filter((obj) => obj.id === parseInt(currentObjId, 10))[0];
    const tempAdrs = data[2].split(';');
    $('#name').val(data[0]);
    $('#last_name').val(data[1]);
    $('#street').val(tempAdrs[0]);
    $('#city').val(tempAdrs[1]);
    $('#country').val(tempAdrs[2]);
    $('#telephone').val(data[3]);
    $('#tags').val(data[4]);
    $('#homepage').val(currentData.urls);
    $('#avatar').removeAttr('hidden');
    $('#avatar').attr('src', currentData.image);
}

function setActive(elem) {
    currentObjId = $(elem).attr('id');
    const data = [
        $(elem).find('.tdName').html(),
        $(elem).find('.tdLastName').html(),
        $(elem).find('.tdAdrs').html(),
        $(elem).find('.tdPhone').html(),
        $(elem).find('.tdTags').html(),
    ];
    $(elem).parent().children("tr").removeClass('active');
    $(elem).addClass("active");
    changeForm(data);
}

function deleteContact(idContact) {
    $.ajax({
        type: 'DELETE',
        url: `/api/contacts/${idContact}/`,
         statusCode: {
            204: function (data) {location.reload()},
        }
    });
}
// Validation request part
//////////////////////////
// CRSF validation
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});