from django.urls import path
from django.views.generic import TemplateView
from src.api.urls import router
from django.conf.urls import include, url
from django.contrib import admin
from src import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/', include((router.urls, 'src'), namespace='api')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-auth/', include('rest_auth.urls')),
    url(r'^home/', TemplateView.as_view(template_name='base.html')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
