from django.contrib.auth.models import User


class CreateTestUserMixin(object):
    """
    Creating mock user for testing
    """

    def setUp(self):
        user_data = {
            'username': 'Tester',
            'email': 'tester@mail.com'
        }
        self.user = User.objects.create(**user_data)
        self.client.force_authenticate(self.user)


class HTTPMethodStatusCodeTestMixin(object):
    """
    Get list of methods, url to check, compare with status code
    """

    def run_method_status_code_check(self, url, methods, status_code):
        """
        Ensure API call to :url: will return :status_code: to request with :methods:.
        """
        for method in methods:
            response = getattr(self.client, method)(url)
            self.assertEqual(response.status_code, status_code)
