from functools import partial
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from src.address_book.models import Contact
from src.api.tests.factories import ContactFactory
from src.api.tests.mixins import CreateTestUserMixin, HTTPMethodStatusCodeTestMixin


class TestContactViewSet(CreateTestUserMixin, HTTPMethodStatusCodeTestMixin, APITestCase):

    def setUp(self):
        super().setUp()
        self.user.is_staff = True
        self.urls = {
            'list': reverse('api:contact-list'),
            'detail': partial(reverse, 'api:contact-detail'),
        }
        self.required_fields = (
            'name',
            'last_name'
        )
        self.long_char = 'Q'*201
        self.data = {
            'valid': {
                'name': 'eduard',
                'last_name': 'kucheryaviy',
                'url': 'google.com'
            },
            'invalid': {
                'name': self.long_char,
                'last_name': self.long_char,
                'url': 'googlecom',
                'telephone': '666',
                'tags': 'string',

            }
        }
        self.additional_invalid_fields = (
            'tags',
            'telephone',
            'url'
        )

    def assertSortedForceListEqual(self, list1, list2, msg=None):
        list1 = sorted(list1)
        list2 = sorted(list2)
        self.assertListEqual(list1, list2, msg)

    def test_create_contact_with_empty_data(self):
        """
        Ensure will not create and returns validator errors
        """
        response = self.client.post(self.urls['list'], {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertSortedForceListEqual(response.data.keys(), self.required_fields)

    def test_create_contact_with_valid_data(self):
        """
        Ensure correct contact will be created in DB.
        """
        data = self.data['valid']
        response = self.client.post(self.urls['list'], data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.contact_from_db = Contact.objects.get(name=data['name'])
        for field_name, value in data.items():
            self.assertEqual(
                getattr(self.contact_from_db, field_name), value
            )
        self.assertTrue(Contact.objects.all().exists())

    def test_create_contact_with_invalid_data(self):
        """
        Ensure incorrect contact won't be created in DB.
        Check that returns errors for every bad field.
        """
        response = self.client.post(self.urls['list'], self.data['invalid'])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertSortedForceListEqual(response.data.keys(),
                                        self.required_fields + self.additional_invalid_fields)
        self.assertFalse(Contact.objects.all().exists())

    def test_not_available_api_list_methods(self):
        """
        Api view will return 405 status code to request with not allowed
        methods: put, patch, delete
        """
        self.run_method_status_code_check(
            url=self.urls['list'],
            methods=['put', 'patch', 'delete'],
            status_code=status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_available_api_list_methods(self):
        """
        Api view will return 200 status code to request with allowed
        methods: get
        """
        self.run_method_status_code_check(
            url=self.urls['list'],
            methods=['get', 'options'],
            status_code=status.HTTP_200_OK
        )

    def test_contact_detail_api_view(self):
        """
        Ensure API return existing object
        """
        contact = ContactFactory()
        url = self.urls['detail'](kwargs={'pk': contact.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
