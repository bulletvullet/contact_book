import factory
from src.address_book.models import Contact


class ContactFactory(factory.DjangoModelFactory):
    """
    Create mock contact object
    """
    name = factory.Sequence(lambda n: "name_{}".format(n))
    last_name = factory.Sequence(lambda n: "last_name_{}".format(n))

    class Meta:
        model = Contact
