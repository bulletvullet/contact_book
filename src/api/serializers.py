from rest_framework import serializers
from ..address_book.models import Contact, Address
from taggit_serializer.serializers import TagListSerializerField


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ('city', 'country', 'street')


class ContactSerializer(serializers.ModelSerializer):
    address = AddressSerializer(required=False)
    tags = TagListSerializerField(required=False)

    class Meta:
        model = Contact
        fields = '__all__'

