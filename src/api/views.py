from rest_framework import viewsets, generics, filters
from ..address_book.models import Contact, Address
from .mixins import MethodSerializerMixin
from .serializers import ContactSerializer


class ContactApiView(generics.ListAPIView):
    """
    ApiView for searching in text fields of Contacts
    """
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        '@name',
        '@last_name',
        '@address__city',
        '@address__country',
        '@address__street',
        '@telephone',
        '@url',
        '@tags__name'
    )


class ContactViewSet(MethodSerializerMixin, viewsets.ModelViewSet, ContactApiView):
    queryset = Contact.objects.all()

    method_serializer_classes = {
        ('GET', 'POST', 'PUT', 'PATCH'): ContactSerializer,
    }

    def perform_create(self, serializer):
        """
        Using for handling Foreign Key address and custom tags field
        """
        kwargs = {}
        address_data = serializer.validated_data.pop('address', default=None)
        tags_data = serializer.validated_data.pop('tags', default=None)
        if address_data and address_data.values():
            kwargs.update({
                'address': Address.objects.create(**address_data)
            })
        instance = serializer.save(**kwargs)
        if tags_data:
            instance.tags.set(*tags_data)
        return instance

    def perform_update(self, serializer):
        """
        Using for handling Foreign Key address and custom tags field
        """
        instance = self.get_object()
        address_data = serializer.validated_data.pop('address', default=None)
        tags_data = serializer.validated_data.pop('tags', default=None)
        if tags_data:
            instance.tags.set(*tags_data)
        for field, value in address_data.items():
            setattr(instance.address, field, value)
        instance.address.save()
        return serializer.save()
