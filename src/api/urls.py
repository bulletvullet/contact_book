from rest_framework.routers import DefaultRouter
from src.api.views import ContactViewSet

router = DefaultRouter()
router.register(r'contacts', ContactViewSet, base_name='contact')
