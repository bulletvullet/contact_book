Simplest contact book.
Used: django, django-rest-framework, docker

## Getting Started
After starting, project will be available:
http://localhost:8000/home/ - home page.
http://localhost:8000/api/ - rest api.
Login: admin, Password: 1

### Prerequisites

docker and docker-compose for your OS.

### Installing

1. Get directory of project and build via docker.

```
$ docker-compose build
```

2. After building > start project.

```
$ docker-compose up
```

## Running the tests

```
$ python manage.py test
```

## Built With

* [django](https://www.djangoproject.com/) - Web framework
* [django-rest-framework](https://www.django-rest-framework.org/) - Web framework
* [docker](https://www.docker.com/) - Enterprise container platform

## Authors

* **Eduard Kucheryaviy** - [bulletvullet](https://bitbucket.org/bulletvullet/)

## License

This project is licensed under the MIT License

